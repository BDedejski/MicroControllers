char keypadPort at PORTD;
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;
sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;

char uart_rd;
char kp;
char uartInput[20];   //karakterite sto se vnesuvaat preku UART da se smestat vo niza da vidime dali ima ADD,ERASE,END
char resetUartInput[20]; //za drug input uartInput ke go davame taa vrednost za da restartirame
int i;                //za polnenje
int j;                //proverka dali sodrzi ADD
int uartInput_Length;
int korisnik_Length;
int k;
int flagStart;       //za da pocne sega za korisnik
int numberOfSpaces;
int u;             //za proveruvanje posle tockata dali ima nesto

char korisnik[20];
char korisnickiBroj[10];
char korisnickaSifra[10];
int g;            //za korisnik


int flagKorisnik=0; //posle OK -> flag=1 pa vnesuvaj nov korisnik
int flagIzbrisan=0;

char sifraBrisenje[20];
int sifraBrisenje_Length;
int matchCounter;

//redovi vo matricata za brisenje
int red0=0x00;
int red1=0x10;
int red2=0x20;
int red;

//
int flagEnd=0;
//promenlivi sto ni trebat vo korisnicki mod
char inputKorisnikBroj[20];
int inputKorisnikBroj_Length;
int counterBroj;            //brojac
int t;
int pogodok;



void main() {
ANSEL=0;
ANSELH=0;
kp = 0;
uartInput[20]=0;
i=0;
g=0;

flagStart=0;
uartInput_Length=0;
korisnik_Length=0;
numberOfSpaces=0;      //125.25(prazno mesto) ako broj na prazni mesta e 3 sme vnesele 3 korisnici

sifraBrisenje[20]=0;   //nizata vo koja ke se smesti sifrata za brisenje
sifraBrisenje_Length=0; //dolzinata na sifrata na brisenje
matchCounter =0;       //ako dostignat MATCHCounter dostigne isto so niza length znaci taa e sifrata i brisaj go eeprom

t=0;
pogodok=0;

red=0x00;


UART1_Init(9600);
Delay_ms(50);

Keypad_Init();
Lcd_Init();
Lcd_Cmd(_LCD_CURSOR_OFF);
Lcd_Cmd(_LCD_CLEAR);



     while(1)
     {
      //OD TUKA POCNUVA ADMINISTRATORSKI MOD
      if(UART1_Data_Ready())
      {
       uart_rd=UART1_Read();
       if(uart_rd!=' ')
       {
        uartInput[i]=uart_rd;
        uartInput_Length++;
        i++;
    //    Lcd_Out(1,1,uartInput);
        EEPROM_Write(0x08,uartInput_Length);
       }
       else if(uart_rd==' ')
       {
            for(j=0;j<uartInput_Length;j++)
            {
             if((uartInput[j]=='A')&&(uartInput[j+1]=='D')&&(uartInput[j+2]=='D'))
               {
                UART1_Write_Text("Start");      //po START vnesuva korisnik, koj ke odi vo eeprom korisnicko ime na 0x08 soodvetna lozina na 0x09
                while(1)                 //koga ke napise start pocnuvame so vnesuvanje na korisnici od tip 125.25' ' (125.25 prazno mesto)
                {
                 if(UART1_Data_Ready())
                 {
                  uart_rd=UART1_Read();

                  if(uart_rd!=' ')
                  {
                   korisnik[g]=uart_rd;
                   g++;

                   korisnik_Length++;

                   if(numberOfSpaces==0)
                   {
                   EEPROM_Write(0x00+g-1,uart_rd);   //prv korisinik pisi go vo prv red na eeprom


                   }
                   else if(numberOfSpaces==1)      //znaci 2r korisnik pisi go vo vtor red na eeprom
                   {
                    EEPROM_Write(0x10+g-1,uart_rd);
                   }

                  }
                  else
                  {
                    for(u=0;u<korisnik_Length;u++)
                   {
                    if((korisnik[u]=='.')&&(korisnik[u+1]>='0' && korisnik[u+1]<='9'))
                    {
                      UART1_Write_Text("OK");
                      flagKorisnik=1;             //vnesen e korisnikot

                    }
                    else if((korisnik[u]=='.')&&(!(korisnik[u+1]>='0' && korisnik[u+1]<='9')))
                    {
                       UART1_Write_Text("ERROR"); //error
                       flagKorisnik=1;            //vnesen e korisnikot
                    }
                   }

                   numberOfSpaces++;
                   g=0;
                   uart_rd=0;
                   korisnik_Length=0;

                   //break;
                  }

                 }
                  //ako e vnesen veke korisnikot so OK ili ERROR, flagkorsnik=0; i izlezi od while(1) pocni od pocetok na programa
                  if(flagKorisnik==1)
                  {
                  flagKorisnik=0;
                  break;
                  }


                }
              //  Lcd_Out(1,1,"sega");
               // Lcd_Cmd(_LCD_CLEAR);
               // Lcd_Out(1,1,korisnik);
               }
               //Ako na input dobieme ERASE brisi korisnik so taa sifra
               else if(((uartInput[j]=='E')&&(uartInput[j+1]=='R')&&(uartInput[j+2]=='A')&&(uartInput[j+3]=='S')&&(uartInput[j+4]=='E')))
               {
                 i=0;
                 UART1_Write_Text("Brisi");
                 //ovde ke pocnuva procesot na brisenje
                 while(1)
                 {
                  if(UART1_Data_Ready())
                  {
                   uart_rd=UART1_Read();
                   if(uart_rd!=' ')
                   {
                    sifraBrisenje[i]=uart_rd;
                    i++;
                    sifraBrisenje_Length++;

                   }
                   //matrica j e na desno i e na dole
                   else if(uart_rd==' ')
                   {
                    for(i=0;i<sifraBrisenje_Length;i++)
                    {
                     for(j=0;j<sifraBrisenje_Length;j++)
                     {
                     if(sifraBrisenje[i]==EEPROM_Read(red+j))
                     {
                       matchCounter++;
                      if(matchCounter==sifraBrisenje_Length)
                      {
                       EEPROM_Write(red,'0');   //KOGA PRVATA CIFRA NA SIFRATA NA KORISNIKOT STANUVA 0, KORISNIKOT E IZBRISAN
                       UART1_Write_Text("OK");
                       flagIzbrisan=1;
                      }

                     }
                     /*   TOA NEZNAM ZOSTO NE RABOTI !
                     else
                     {
                      red=red+0x10;
                     }
                     */
                     else if(sifraBrisenje[i]==EEPROM_Read(red1+j))
                     {
                       matchCounter++;
                      if(matchCounter==sifraBrisenje_Length)
                      {
                       EEPROM_Write(red1,'0');   //KOGA PRVATA CIFRA NA SIFRATA NA KORISNIKOT STANUVA 0, KORISNIKOT E IZBRISAN
                       UART1_Write_Text("OK");
                       flagIzbrisan=1;
                      }
                     }
                     }

                    }

                      sifraBrisenje_Length=0;
                      uart_rd=0;
                       matchCounter=0;
                      i,j=0;


                   }
                  }
                    if(flagIzbrisan==1)
                    {

                      for(k=0;k<20;k++)
                      {
                      sifraBrisenje[k]=0;
                      }
                      break;
                    }
                 }
                  if(flagIzbrisan==1)
                  {
                   flagIzbrisan=0;
                   break;
                  }
               }
               //zavrsuva ERASE
               else if((uartInput[j]=='E')&&(uartInput[j+1]=='N')&&(uartInput[j+2]=='D'))
               {
                   flagEND=1;
               }
            }

            for(k=0;k<20;k++)
            {
             uartInput[k]=0;
            }
        uart_rd=0;
        uartInput_Length=0;
        i=0;
       }

      }
        //izleguvame od toj mod vleguvame vo korisnicki
        if(flagEnd==1)
        {
         break;
        }
     }
      //OD TUKA POCNUVA KORISNICKI MOD

      
      if(flagEnd==1)
      {
       Lcd_Cmd(_LCD_CLEAR);
       Lcd_Out(1,1,"vnesi broj");
       i=0;

       while(1)
       {

        do
        kp = Keypad_Key_Click();
        while(!kp);


        switch(kp){
         case 1: kp = '1'; break;
         case 2: kp = '4'; break;
         case 3: kp = '7';  break;
         case 4: kp = 'P';  break;  //proveri dali korisnickoto ime i lozinkata e validno
         case 5: kp = '2';  break;
         case 6: kp = '5';  break;
         case 7: kp = '8';  break;
         case 8: kp = '.';  break;  //za posle sifra
         case 9: kp = '3';  break;
         case 10: kp = '6'; break;
         case 11: kp = '9'; break;
         case 12: kp = 'B'; break;  //BACK
         case 13: kp = 'I'; break; //ID
         case 14: kp = 'C'; break; //CODE
         case 15: kp = 'E'; break; //EXIT
         case 16: kp = 'R'; break; //ENTER
         }
         if((kp!='R')&& (kp!='C') && (kp!='B') && (kp!='I') && (kp!='E') &&(kp!='P'))
         {
           inputKorisnikBroj[i]=kp;
           i++;
           counterBroj++;          //kolku e dolga sifrata
           t++;
           if(kp==EEPROM_Read(red+t-1))
           {
            pogodok++;
           }

         }
         else if(kp=='R')
         {
          Lcd_Cmd(_LCD_CLEAR);
          Lcd_Out(1,1,inputKorisnikBroj);
         }
         else if(kp=='B')
         {
          i--;
         }
         else if(kp=='P')
         {
           if(pogodok==counterBroj)
           {
            Lcd_Cmd(_LCD_CLEAR);
            Lcd_Out(1,1,"validna avtorizacija");
           // EEPROM_Write(0x80,'t');
           }
              break;
         }
       }
      }


}














/*
    testiranje, vnesuvas od UART borisADD (prazno mesto)
    -> koga ke napise Start vnesuvas korisnik vo tip 125.25 (prazno mesto) -> pecati OK
    -> koga ke napise Start vneuvas korisnik tip 125. (prazno mesto) -> pecati ERROR nema nisto posle tocka
       //ERASE
       TESTIRANJE 2
       aADD -> (START) -> 125.25 (PRAZNO) (OK)
       bADD -> (START) -> 34.25 (PRAZNO)(OK)
       aERASE -> (BRISI) -> 34 (OK)
       bERASE -> (BRISI) -> 125 (OK) BRISENJETO GO SMETAME KOGA PRVATA CIFRA OD SIFRATA VO EEPROM E 0


  */